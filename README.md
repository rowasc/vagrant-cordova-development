# README #

This Vagrant configuration will help you setup a Phonegap environment in minutes. 

### What is this repository for? ###

* This project is meant to help you setting up a new Phonegap project
* Version 0.1.0
* [Blog](http://www.richardruiter.nl/category/development/vagrant-development/phonegap-vagrant-development/)

### How do I get set up? ###

* You will need to install Vagrant and a virtualization platform (like Virtualbox)
* Clone this repo and run: vagrant up